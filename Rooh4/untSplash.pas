unit untSplash;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, jpeg, ExtCtrls, ActnList, MPlayer, StdCtrls, Buttons, VolCtrl,
  OleCtrls, ShockwaveFlashObjects_TLB, AppEvnts, Menus;

type
  TfrmSplash = class(TForm)
    ActionList1: TActionList;
    actCloseSplash: TAction;
    ApplicationEvents1: TApplicationEvents;
    ppmSplash: TPopupMenu;
    swf1: TShockwaveFlash;
    procedure FormCreate(Sender: TObject);
    procedure actCloseSplashExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure swf1FSCommand(ASender: TObject; const command,
      args: WideString);
    procedure FormShow(Sender: TObject);
    procedure ApplicationEvents1Message(var Msg: tagMSG;
      var Handled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSplash: TfrmSplash;
  swfMovie: String;

implementation

uses untMain;

{$R *.dfm}

procedure TfrmSplash.FormCreate(Sender: TObject);
begin
  WindowState := wsMaximized;
end;

procedure TfrmSplash.actCloseSplashExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmSplash.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmSplash.swf1FSCommand(ASender: TObject; const command,
  args: WideString);
begin
//  try
//  if LowerCase(command) = 'click' then
  Close;
//  except;end;
end;

procedure TfrmSplash.FormShow(Sender: TObject);
var
  De: TCanvas;
begin
  De := TCanvas.Create;
  De.Handle := GetDC(HWND_DESKTOP);
  swf1.Free;
  with TShockwaveFlash.Create(Application) do
  begin
    Name := 'swf1';
    Left := (De.ClipRect.Right - 800) div 2;
    Top := (De.ClipRect.Bottom - 600) div 2;
    Width := 800;
    Height := 600;
    ScaleMode := 3;
    Movie := swfMovie;
    OnFSCommand := swf1FSCommand;
    TabStop := False;
    Loop := False;
    
    Parent := frmSplash;
  end;

  ApplicationEvents1.OnMessage := ApplicationEvents1Message;
end;

procedure TfrmSplash.ApplicationEvents1Message(var Msg: tagMSG;
  var Handled: Boolean);
begin
  if (Msg.message = WM_RBUTTONDOWN) // and (Msg.hwnd = swf1.Handle)
     then
  begin
    ppmSplash.Popup(0,0);
    Handled := True;
  end;
end;

end.
